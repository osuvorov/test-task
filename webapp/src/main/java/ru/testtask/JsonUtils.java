package ru.testtask;

import javax.json.JsonObject;
import javax.json.JsonValue;
import javax.json.JsonValue.ValueType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Contains methods for Json parsing
 */
public class JsonUtils {

	private static final Logger log = LoggerFactory.getLogger(IConstants.APP_LOGGER);

	/**
	 * Tries to parse value with the specified name of the specified object to
	 * string.
	 * 
	 * @param object
	 *            an object to be searched for the property
	 * @param propertyName
	 *            property name
	 * @return string value or <code>null</code> if no such property is defined
	 *         for the object
	 */
	public static String parseJsonString(JsonObject object, String propertyName) {
		if (propertyName == null) {
			log.error("[json] Cannot parse string property: name is not specified!");
			throw new NullPointerException("property's name");
		}
		if (object == null) {
			log.error("[json] Cannot parse string property: object is not specified!");
			throw new NullPointerException("object to be parsed");
		}
		if (!object.containsKey(propertyName)) {
			log.warn("[json] No property with name {} defined for the object", propertyName);
			return null;
		}
		JsonValue value = object.get(propertyName);
		if (value.getValueType() == ValueType.STRING) {
			return object.getString(propertyName);
		} else if (value.getValueType() == ValueType.NUMBER) {
			return Integer.toString(object.getInt(propertyName));
		} else if (value.getValueType() == ValueType.NULL) {
			return null;
		} else if (value.getValueType() == ValueType.FALSE) {
			return Boolean.FALSE.toString();
		} else if (value.getValueType() == ValueType.TRUE) {
			return Boolean.TRUE.toString();
		} else {
			log.error("[json] Cannot parse {} property to string: invalid type {}", propertyName,
					value.getValueType().toString());
			return null;
		}
	}
}