package ru.testtask;

/**
 * Represents a format of an input text.
 */
public enum InputFormat {

	PLAIN("text/plain");

	private String mimeType;

	private InputFormat(String mimeType) {
		this.mimeType = mimeType;
	}

	/**
	 * Returns MIME type of the format.
	 * 
	 * @return type string
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * Returns input format according to the specified MIME type string.
	 * 
	 * @param type
	 * @return appropriate {@link InputFormat} element or {@code null} if none
	 *         has been found
	 */
	public static InputFormat getFormatByMIMEString(String type) {
		for (InputFormat format : InputFormat.values()) {
			if (format.getMimeType().equals(type.toLowerCase())) {
				return format;
			}
		}
		return null;
	}

}