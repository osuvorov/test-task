package ru.testtask;

/**
 * Contains the constants for the application.
 * 
 */
public interface IConstants {

	/** Name of the root logger for the application */
	String APP_LOGGER = "app";

	/**
	 * The default encoding.
	 */
	String DEFAULT_ENCODING = "UTF-8";
	/**
	 * The default content type for both input(request) and output(response).
	 */
	String DEFAULT_CONTENT_TYPE = "application/json";

	/**
	 * The servlet parameter for the MIME type of the source text.
	 */
	String PARAM_SRCTEXTTYPE = "srctype";

}
