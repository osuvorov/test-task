package ru.testtask.plain.model;

import javax.json.JsonStructure;

/**
 * Defines methods for every object that can be exported as JSON.
 */
public interface IExportable {
	/**
	 * Creates a JSON structure for this model object.
	 * 
	 * @return a JSON structure
	 */
	JsonStructure toJson();
}
