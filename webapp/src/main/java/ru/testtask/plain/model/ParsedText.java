package ru.testtask.plain.model;

import java.util.LinkedList;
import java.util.List;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;

/**
 * Represents a parsed text as a list of chapters.
 */
public class ParsedText implements IExportable {

	private List<Chapter> chapters;

	/**
	 * Creates a new parsed text with an empty chapter list.
	 */
	public ParsedText() {
		this.chapters = new LinkedList<>();
	}

	/**
	 * Adds a chapter to the text.
	 * 
	 * @param chapter
	 *            chapter to be added
	 */
	public void addChapter(Chapter chapter) {
		chapters.add(chapter);
	}

	/**
	 * Returns the list of all chapters of the text
	 * 
	 * @return list of chapters
	 */
	public List<Chapter> getAllChapters() {
		return chapters;
	}

	@Override
	public JsonArray toJson() {
		JsonArrayBuilder builder = Json.createArrayBuilder();

		if (chapters != null && !chapters.isEmpty()) {
			for (Chapter chapter : chapters) {
				builder.add(chapter.toJson());
			}
		}

		return builder.build();
	}
}