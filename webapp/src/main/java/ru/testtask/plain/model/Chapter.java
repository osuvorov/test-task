package ru.testtask.plain.model;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;

import ru.testtask.plain.IConstants;

/**
 * Represents a chapter in parsed text.
 */
public class Chapter implements IExportable {

	private String title;
	private String text;

	/**
	 * Creates a new chapter with the specified title and content.
	 * 
	 * @param title
	 *            the title of the chapter
	 * @param text
	 *            the content of the chapter
	 * @throws NullPointerException
	 *             if the title is not specified
	 */
	public Chapter(String title, String text) {

		if (title == null) {
			throw new NullPointerException("chapter's title");
		}

		this.title = title;
		this.text = text;

	}

	/**
	 * Sets the title for the chapter.
	 * 
	 * @param title
	 *            chapter's title
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * Returns the title of the chapter
	 * 
	 * @return capter's title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the content of the chapter.
	 * 
	 * @param text
	 *            chapter's content
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * Returns the content of the chapter.
	 * 
	 * @return chapter's content
	 */
	public String getText() {
		return text;
	}

	@Override
	public JsonObject toJson() {
		
		JsonObjectBuilder builder = Json.createObjectBuilder();

		if (title != null) {
			builder.add(IConstants.JSON_CHAPTER_TITLE, title);
		} else {
			builder.addNull(IConstants.JSON_CHAPTER_TITLE);
		}

		if (text != null) {
			builder.add(IConstants.JSON_CHAPTER_CONTENT, text);
		}
		return builder.build();
	}

}
