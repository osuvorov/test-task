package ru.testtask.plain.model;

import javax.json.JsonObject;

import ru.testtask.JsonUtils;
import ru.testtask.plain.IConstants;

/**
 * Source unparsed text.
 */
public class SourceText {

	private String content;

	/**
	 * Creates new source text with the specified content.
	 * 
	 * @param text
	 *            content of the text
	 */
	public SourceText(String text) {
		this.content = text;
	}

	/**
	 * Returns the content of the text.
	 * 
	 * @return text content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content of the text.
	 * 
	 * @param content
	 *            text content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Creates unparsed text from the specified JSON object.
	 * 
	 * @param jsonObject
	 *            source JSON object
	 * @return source text or {@code null} if
	 *         {@link IConstants.JSON_SOURCETEXT_CONTENT} was not found
	 */
	public static SourceText parseSourceText(JsonObject jsonObject) {

		if (jsonObject.containsKey(IConstants.JSON_SOURCETEXT_CONTENT)) {

			return new SourceText(JsonUtils.parseJsonString(jsonObject, IConstants.JSON_SOURCETEXT_CONTENT));

		} else {

			return null;

		}
	}
}