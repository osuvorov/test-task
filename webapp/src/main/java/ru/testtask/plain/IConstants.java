package ru.testtask.plain;

/**
 * Constants for plain text format.
 */
public interface IConstants {

	/* JSON constants */

	String JSON_SOURCETEXT_CONTENT = "text";

	String JSON_CHAPTER_TITLE = "chapter_name";
	String JSON_CHAPTER_CONTENT = "chapter_text";

}
