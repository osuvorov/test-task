package ru.testtask.plain.parsers;

import ru.testtask.plain.model.Chapter;
import ru.testtask.plain.model.ParsedText;

/**
 * Listens to chapter events and creates a {@link ParsedText} object.
 */
public class DefaultChapterEventListener implements IChapterEventListener {

	private ParsedText text;
	private Chapter currentChapter;
	private StringBuilder currentChapterContent;

	/**
	 * Creates a new listener.
	 */
	public DefaultChapterEventListener() {
		text = new ParsedText();
		currentChapterContent = new StringBuilder();
	}

	@Override
	public void endOfText() {
		if (currentChapter != null && currentChapterContent.length() > 0) {
			currentChapter.setText(currentChapterContent.toString());
		}
	}

	public ParsedText getParsedText() {
		return text;
	}

	@Override
	public void emptyLine() {
	}

	@Override
	public void newChapter(String chapterTitle) {
		if (currentChapter != null && currentChapterContent.length() > 0) {
			currentChapter.setText(currentChapterContent.toString());
		}
		currentChapterContent.setLength(0);
		currentChapter = new Chapter(chapterTitle, "");
		text.addChapter(currentChapter);
	}

	@Override
	public void text(String chapterContent) {
		currentChapterContent.append(chapterContent);
	}
}
