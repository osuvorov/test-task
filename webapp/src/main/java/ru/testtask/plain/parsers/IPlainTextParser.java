package ru.testtask.plain.parsers;

import ru.testtask.FormatException;
import ru.testtask.plain.model.SourceText;

/**
 * Defines methods for plain text parser.
 */
public interface IPlainTextParser {

	/**
	 * Parses the specified text.
	 * 
	 * @param unparsedText
	 *            the source text to parse
	 * @throws FormatException
	 */
	void parse(SourceText unparsedText) throws FormatException;

}