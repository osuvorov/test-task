package ru.testtask.plain.parsers;

import java.util.LinkedList;
import java.util.List;

import ru.testtask.FormatException;
import ru.testtask.InputFormat;
import ru.testtask.plain.model.SourceText;
import ru.testtask.plain.rules.PlainTextUtils;

/**
 * Reads the source text line by line using the {@link IChapterEventListener}.
 */
public class DefaultPlainTextParser implements IPlainTextParser {

	private List<IChapterEventListener> listeners;

	private String currentChapterName;

	private int currentLineNumber = 1;

	/**
	 * Creates a new instance of the parser.
	 */
	public DefaultPlainTextParser() {
		listeners = new LinkedList<IChapterEventListener>();
	}

	@Override
	public void parse(SourceText unparsedText) throws FormatException {
		if (unparsedText == null) {
			throw new NullPointerException("unparsed text's content");
		}

		StringBuilder buffer = new StringBuilder(unparsedText.getContent());

		int iStart = 0, length = buffer.length();

		// detect any possible line terminators (but should be OK, since they
		// are removed in the servlet code)
		for (int i = 0; i < length; i++) {
			if (buffer.charAt(i) == '\r' || buffer.charAt(i) == '\n') {
				processLine(buffer.substring(iStart, i));
				if ((i + 1 < length) && (buffer.charAt(i + 1) == '\n')) {
					i++;
				}
				iStart = i + 1;
			}
		}

		// process trailing text if any
		if (iStart < length) {
			processLine(buffer.substring(iStart, length));
		}

		fireEndOfText();
	}

	/**
	 * Adds the specified listener.
	 * 
	 * @param listener
	 */
	public void addListener(IChapterEventListener listener) {
		synchronized (listeners) {
			if (!listeners.contains(listener)) {
				listeners.add(listener);
			}
		}

	}

	/**
	 * Removes the specified listener.
	 * 
	 * @param listener
	 */
	public void removeListener(IChapterEventListener listener) {
		synchronized (listeners) {
			listeners.remove(listener);
		}

	}

	/**
	 * Removes all listeners that were added to this parser.
	 */
	public void removeAllListeners() {
		synchronized (listeners) {
			listeners.clear();
		}
	}

	private void processLine(String line) throws FormatException {
		String text = PlainTextUtils.trimLine(line);

		int lineLength = text.length();
		int iLastDot = text.lastIndexOf('.');

		if (lineLength == 0) {
			// the line is empty - any chapter ends
			currentChapterName = null;
		} else if (iLastDot == lineLength - 1) {
			// chapter text
			if (currentChapterName == null) {
				throw new FormatException(InputFormat.PLAIN, "Untitled chapter at line " + currentLineNumber);
			} else {
				fireChapterText(PlainTextUtils.trimLine(text));
			}
		} else if (iLastDot < 0) {
			// chapter title
			currentChapterName = PlainTextUtils.trimLine(text);
			fireNewChapter(currentChapterName);
		} else {
			throw new FormatException(InputFormat.PLAIN,
					"Neither chapter title nor chapter text found at line " + currentLineNumber);
		}

		currentLineNumber++;
	}

	private void fireEndOfText() {
		for (IChapterEventListener listener : listeners) {
			listener.endOfText();
		}
	}

	private void fireNewChapter(String chapterTitle) {
		for (IChapterEventListener listener : listeners) {
			listener.newChapter(chapterTitle);
		}
	}

	private void fireChapterText(String chapterContent) {
		for (IChapterEventListener listener : listeners) {
			listener.text(chapterContent);
		}
	}
}
