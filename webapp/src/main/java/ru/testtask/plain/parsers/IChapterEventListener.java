package ru.testtask.plain.parsers;

/**
 * Defines methods of listener for a new line event.
 */
public interface IChapterEventListener {

	/**
	 * Indicates that a parser reached the end of the text.
	 */
	void endOfText();

	/**
	 * Indicates that an empty line has been read.
	 */
	void emptyLine();

	/**
	 * Indicates a start of a new chapter.
	 * 
	 * @param chapterTitle
	 *            the name of the chapter
	 */
	void newChapter(String chapterTitle);

	/**
	 * Indicates that a chunk of a chapter text has been read.
	 * 
	 * @param chapterContent
	 */
	void text(String chapterContent);
}