package ru.testtask.plain.rules;

/**
 * Contains utility methods for working with lines of plain text.
 */
public class PlainTextUtils {

	/**
	 * Array of characters that are not meaning both at the beginning and at the
	 * end of a line.
	 */
	public static final String TRIM_CHARS = "@*# ";

	/**
	 * Checks whether the specified character should be removed.
	 * 
	 * @param c
	 *            the character to be tested
	 * @return {@code true} if the character can be removed, {@code false}
	 *         otherwise
	 */
	public static boolean isTrimCharacter(char c) {
		return TRIM_CHARS.indexOf(c) >= 0;
	}

	/**
	 * Removes unnecessary characters at the beginning and at the end of the
	 * specified string.
	 * 
	 * @param source
	 *            the string to be trimmed
	 * @return
	 */
	public static String trimLine(String source) {
		StringBuilder result = new StringBuilder(source);
		int iStart = -1, iEnd = result.length() - 1;
		// remove characters from the beginning
		for (int i = 0; i <= iEnd; i++) {
			if (!PlainTextUtils.isTrimCharacter(result.charAt(i))) {
				iStart = i;
				break;
			}
		}

		// if no such characters - the string has no meaning data
		if (iStart < 0) {
			return "";
		}

		// remove characters from the end
		for (int i = iEnd; i >= 0; i--) {
			if (!PlainTextUtils.isTrimCharacter(result.charAt(i))) {
				iEnd = i + 1;
				break;
			}
		}

		return result.substring(iStart, iEnd);
	}

}
