package ru.testtask.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.concurrent.Callable;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.testtask.IConstants;
import ru.testtask.InputFormat;
import ru.testtask.plain.model.SourceText;
import ru.testtask.plain.parsers.DefaultChapterEventListener;
import ru.testtask.plain.parsers.DefaultPlainTextParser;

/**
 * Handler for client requests.
 */
public class ParseRequestHandler implements Callable<String> {

	private static final Logger log = LoggerFactory.getLogger(IConstants.APP_LOGGER);

	private static final String DEFAULT_CHARSET = "UTF-8";

	private String sourceText;
	private InputFormat inputFormat;

	/**
	 * Creates a new handler for the specified request.
	 * 
	 * @param request
	 *            the request for parsing
	 * @throws ServiceException
	 *             if the source text type is not specified or unknown or cannot
	 *             be read
	 * @throws NullPointerException
	 *             if the {@code request} is {@code null}
	 */
	public ParseRequestHandler(HttpServletRequest request) throws ServiceException {
		if (request == null) {
			throw new NullPointerException("httprequest");
		}

		String encoding = request.getCharacterEncoding();
		if (encoding == null) {
			encoding = DEFAULT_CHARSET;
		} else {
			log.info("Using {} charset (from the request)", encoding);
		}

		// get input format from the request parameters

		String sourceTextType = null;
		if (request.getParameterValues(IConstants.PARAM_SRCTEXTTYPE) == null) {
			throw new ServiceException("Input format is not specified!");
		}
		for (String val : request.getParameterValues(IConstants.PARAM_SRCTEXTTYPE)) {
			sourceTextType = val;
			break;
		}
		if (sourceTextType == null) {
			throw new ServiceException("Input format is not specified!");
		}
		inputFormat = InputFormat.getFormatByMIMEString(sourceTextType);
		if (inputFormat == null) {
			throw new ServiceException("Unknown input format: " + sourceTextType);
		}

		// read the request body

		StringBuilder buffer = new StringBuilder();
		BufferedReader reader = null;
		String line = null;
		try {
			reader = request.getReader();
			while ((line = reader.readLine()) != null) {
				buffer.append(line).append("\n");
			}
		} catch (IOException ex) {
			throw new ServiceException("Cannot read the request body: " + ex.getMessage(), ex);
		}
		sourceText = buffer.toString();
	}

	@Override
	public String call() throws Exception {

		String output = null;

		if (InputFormat.PLAIN == inputFormat) {

			DefaultChapterEventListener listener = new DefaultChapterEventListener();

			DefaultPlainTextParser plainTextParser = new DefaultPlainTextParser();
			plainTextParser.addListener(listener);
			plainTextParser.parse(new SourceText(sourceText));

			output = listener.getParsedText().toJson().toString();

		} else {
			log.error("The processing for the specified input format ({}) is not defined!", inputFormat.getMimeType());
		}

		return output;
	}

}