package ru.testtask.service;

/**
 * The {@link ServiceException} is thrown when the server is not able to handle
 * a user's request either due to the server failure or the request
 * inconsistency.
 */
public class ServiceException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * Creates a new exception.
	 * 
	 * @param message
	 *            detailed message
	 * @param cause
	 *            the cause
	 */
	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Creates a new exception.
	 * 
	 * @param message
	 *            detailed message
	 */
	public ServiceException(String message) {
		super(message);
	}

}
