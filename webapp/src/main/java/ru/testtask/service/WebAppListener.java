package ru.testtask.service;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import ru.testtask.Application;

/**
 * Listens for the application events.
 */
public class WebAppListener implements ServletContextListener {

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		Application.THIS.initialize();
	}

	public void contextDestroyed(ServletContextEvent sce) {
		Application.THIS.terminate();
	};
}