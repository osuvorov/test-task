package ru.testtask.service;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ru.testtask.Application;
import ru.testtask.IConstants;

/**
 * Receives the user's request for parsing, starts a new job and waits for the
 * result. This must be a thread-safe servlet.
 */
@WebServlet(description = "Receives source files", urlPatterns = { "/parse" })
public class ParseServlet extends HttpServlet {

	private static final Logger log = LoggerFactory.getLogger(IConstants.APP_LOGGER);

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.getWriter().append("Please use POST request to submit your data for parsing.");
		response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Future<String> result = null;

		if (Application.THIS.getPool() != null) {
			try {
				result = Application.THIS.getPool().submit(new ParseRequestHandler(request));

				try {
					response.getWriter().append(result.get());
					response.setStatus(HttpServletResponse.SC_OK);
					response.setCharacterEncoding(IConstants.DEFAULT_ENCODING);
					response.setContentType(IConstants.DEFAULT_CONTENT_TYPE);
					return;
				} catch (InterruptedException ex) {
					response.getWriter().append("Unable to parse request: ").append(ex.getMessage());
					ex.printStackTrace(response.getWriter());
					log.error("Unable to parse request", ex);
				} catch (ExecutionException ex) {
					response.getWriter().append("Unable to parse request: ").append(ex.getMessage());
					ex.printStackTrace(response.getWriter());
					log.error("Unable to parse request", ex);
				}

			} catch (ServiceException ex) {
				response.getWriter().append("Unable to parse request: ").append(ex.getMessage());
				ex.printStackTrace(response.getWriter());
				log.error("Unable to parse request", ex);
			}

		} else {
			response.getWriter().append("Unable to parse request: the application was not initialized!");
			log.error("Cannot get the thread pool: the application might not have been started appropriately!");
		}

		response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
	}

	@Override
	public void init() throws ServletException {
		super.init();
	}

	@Override
	public void destroy() {
		super.destroy();
	}
}