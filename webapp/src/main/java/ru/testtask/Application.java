package ru.testtask;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Represents the application core.
 * 
 */
public enum Application {
	THIS;

	private final Logger log = LoggerFactory.getLogger(IConstants.APP_LOGGER);

	private ExecutorService pool;

	private Application() {
		log.trace("Starting the application...");
	}

	/**
	 * Initializes the application.
	 */
	public synchronized void initialize() {
		pool = Executors.newCachedThreadPool();
		log.trace("The application has been started successfully!");
	}

	/**
	 * Terminates application and frees all resources.
	 */
	public synchronized void terminate() {
		pool.shutdown();
		try {
			pool.awaitTermination(5, TimeUnit.SECONDS);
		} catch (InterruptedException ex) {
			log.warn("The thread pool was not shutdown in a reasonable time: please check the timeout settings");
		}
		log.trace("The application has been terminated.");
	}

	/**
	 * Returns the executors pool.
	 * 
	 * @return the thread pool or {@code null} if the application was not
	 *         initialized
	 */
	public ExecutorService getPool() {
		return pool;
	}

}