package ru.testtask;

/**
 * Is thrown when a parsed file does not match the specified format.
 */
public class FormatException extends Exception {
	private static final long serialVersionUID = -3837501255505029216L;

	private InputFormat inputFormat;

	/**
	 * Creates a new exception for the specified input format.
	 * 
	 * @param inputFormat
	 *            the format of the input text
	 * @param message
	 *            an error message
	 * @param cause
	 *            the cause of the exception
	 */
	public FormatException(InputFormat inputFormat, String message, Throwable cause) {
		super(message, cause);
		this.inputFormat = inputFormat;
	}

	/**
	 * Creates a new exception for the specified input format.
	 * 
	 * @param inputFormat
	 *            the format of the input text
	 * @param message
	 *            an error message
	 */
	public FormatException(InputFormat inputFormat, String message) {
		super(message);
		this.inputFormat = inputFormat;
	}

	/**
	 * Returns the input format.
	 * 
	 * @return the format of the input text the parse exception occurred on.
	 */
	public InputFormat getInputFormat() {
		return inputFormat;
	}
}